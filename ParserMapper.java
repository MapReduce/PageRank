package wiki;


import java.io.IOException;
import java.io.StringReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import wiki.Bz2WikiParser.WikiParser;
import wiki.PRChain.NC;

public class ParserMapper extends Mapper<Object, Text, Text, PRNode> {
	// Configure parser.
				
	private Pattern namePattern;
	//private Pattern linkPattern;
	private SAXParserFactory spf;
	private SAXParser saxParser;
	private List<String> linkPageNames;
	private XMLReader xmlReader;
	//private int count;
	public void setup(Context ctx) {
		// Keep only html pages not containing tilde (~).
		namePattern = Pattern.compile("^([^~]+)$");
		// Keep only html filenames ending relative paths and not containing tilde (~).
		//linkPattern = Pattern.compile("^\\..*/([^~]+)\\.html$");
		spf = SAXParserFactory.newInstance();
		try {
			spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			saxParser = spf.newSAXParser();
			xmlReader = saxParser.getXMLReader();
			
		} catch (ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Parser fills this list with linked page names.
		linkPageNames = new LinkedList<String>();
		xmlReader.setContentHandler(new WikiParser(linkPageNames));
		//count = 0;
	}
	
	
    public void map(Object _k, Text tline, Context ctx) throws InterruptedException, IOException {
    	String line = tline.toString();
    	int delimLoc = line.indexOf(':');
    	
		String pageName = line.substring(0, delimLoc);
		String html = line.substring(delimLoc + 1);
		Matcher matcher = namePattern.matcher(pageName);
		if (!matcher.find()) {
			// Skip this html file, name contains (~).
			return;
		}

		// Parse page and fill list of linked pages.
		linkPageNames.clear();
		try {
			xmlReader.parse(new InputSource(new StringReader(html)));
		} catch (Exception e) {
			// Discard ill-formatted pages.
			return;
		}
		//remove self loops
		Set<String> uniqueset = new HashSet<String>(linkPageNames);
		uniqueset.remove(pageName);
		//Populate the Node class.
		PRNode pn = new PRNode();
		pn.adjList = new LinkedList<String>(uniqueset);
		pn.pagerank = 1.0;
		pn.isNode = true;
		ctx.getCounter(NC.NODES).increment(1);
		ctx.write(new Text(pageName), pn);
  
    }
}
 
