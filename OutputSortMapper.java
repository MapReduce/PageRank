package wiki;

import java.io.IOException;
import java.util.TreeMap;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class OutputSortMapper extends Mapper<Text, PRNode, DoubleWritable, Text> {
	//treemap orders entries by key.
	TreeMap<Double, String> top;
	@Override
	public void setup(Context ctx) {
		top = new TreeMap<Double,String>();
	}
	@Override
	public void cleanup(Context ctx) throws IOException, InterruptedException {
		//pass each entry to reducer
		for(double d : top.keySet()){
			ctx.write(new DoubleWritable(d),new Text(top.get(d)));
		}
		//ctx.write(NullWritable.get(), top);
	}
	@Override
	public void map(Text nid, PRNode node, Context ctx) throws InterruptedException, IOException {
		top.put(node.getPagerank(), nid.toString());
		//populate local top 100 and pass it to reducer.
		if(top.size() > 100){
			//we already have 100 elements so remove the least.
			top.remove(top.firstKey());
		}
	}
}
