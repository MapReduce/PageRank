
import java.io.StringReader
import java.util
import java.util.regex.Pattern
import javax.xml.parsers.SAXParserFactory

import Bz2WikiParser._
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.xml.sax.InputSource

import scala.collection.JavaConversions._

object PageRank {

  def getTuple(line : String): Tuple = {
    val linkPageNames: util.List[String] = new util.LinkedList[String]
    var pageName: String = null
    val t = new Bz2WikiParser.Tuple
    val spf = SAXParserFactory.newInstance
    val namePattern = Pattern.compile("^([^~]+)$")
    try {
      val delimLoc = line.indexOf(':')
      pageName = line.substring(0, delimLoc)
      val html = line.substring(delimLoc + 1)
      val matcher = namePattern.matcher(pageName)
      if (!matcher.find) {
        // Skip this html file, name contains (~).
        return t
      }
      spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
      val saxParser = spf.newSAXParser
      val xmlReader = saxParser.getXMLReader
      // Parser fills this list with linked page names.
      xmlReader.setContentHandler(new Bz2WikiParser.WikiParser(linkPageNames))
      linkPageNames.clear()
      xmlReader.parse(new InputSource(new StringReader(html)))
      val uniqueset: util.HashSet[String] = new util.HashSet[String](linkPageNames)
      val linkpnames: util.LinkedList[String] = new util.LinkedList[String](uniqueset)
      t.setPname(pageName)
      t.setLinks(linkpnames)
    }
    catch {
      case e: Exception =>
        //e.printStackTrace()
    }
    t
  }

  def main(args: Array[String]) {

    val conf = new SparkConf().setAppName("PageRank").setMaster("local")
    val sc = new SparkContext(conf)

    //parsed RDD
    //val graph = parser(sc, args(0))
    val bzfile = sc.textFile(args(0))

    //dAcc - initially(in parsing) it counts dangling nodes. and later it computes delta
    //iprAcc - to check if pagerank sums up to 1.0
    //NC - node counter.
    var dAcc = sc.doubleAccumulator("Dangling")
    var ipracc = sc.doubleAccumulator("IPR")
    var NC = sc.longAccumulator("NodeCount")

    val graph = bzfile.map(xs => {
      val tuple = getTuple(xs)
      val pname = tuple.getPname
      val links = tuple.getLinks
      /*if(pname != null){
        NC.add(1)
      }
      if(links != null && links.size() == 0){
        dAcc.add(1.0)
      }*/
      (tuple.getPname,tuple.getLinks)
    }).filter(xs => xs._1 != null)

    //initial page rank setting
    var N = graph.count()
    var initpr = 1.0/N
    var ranks = graph.mapValues(xs => (xs, initpr))

    dAcc.reset()
    //initial delta
    ranks.foreach(xs => {ipracc.add(xs._2._2); if (xs._2._1.size() == 0) {dAcc.add(xs._2._2);}})

    println("SriramD"+dAcc.value+"Init:"+ipracc.value)

    val maxItr = 10

    //10 iteration of page ranks
    for(i <- 1 to maxItr) {

      var delta = dAcc.value
      //contribution of one page to each of its adjacency pages.
      //finally reduce by key so that we get the page rank sum of each page.
      var contrib = ranks.flatMap(xs => {
        var links = xs._2._1
        //var length = xs._2._1.size()
        //var pr = xs._2._2
        //var cont = pr/length
        var cont = xs._2._2/xs._2._1.size()
        val ret = for (link <- links) yield {
          (link, cont)
        }
        ret
      }).keyBy { xs => xs._1 }.reduceByKey((x, y) => (x._1, x._2 + y._2))

      //This will recover the RDD that is going to be used for next iteration
      var recovery = ranks.leftOuterJoin(contrib).mapValues(xs => {
        var prsum = 0.0
        if (xs._2 != None) {prsum = xs._2.get._2}
        var pr = (0.15 / N) + 0.85 * ((delta / N) + prsum)
        (xs._1._1, pr)
      })
      recovery.saveAsTextFile(args(1)+i)
      dAcc.reset()
      ipracc.reset()
      //compute delta
      recovery.foreach(xs => {ipracc.add(xs._2._2); if (xs._2._1.size() == 0) {dAcc.add(xs._2._2);}})
      println("Sriram+" + dAcc.value+"pagerank"+ipracc.value)

      //for the next iteration.
      ranks = recovery

      //final iteration retrieve top100
      if (i == maxItr){

        val PRAcc = sc.doubleAccumulator("PageRank")
        //map only (string) PageName and (Double) PageRank
        val finalPR = recovery.map(xs => (xs._1, xs._2._2))
        finalPR.foreach(xs => PRAcc.add(xs._2))
        //retrieve top 100. reverse the order for 100 top page ranks
        val top100 = finalPR.takeOrdered(100)(Ordering[Double].reverse.on(xs => xs._2))
        //store into final output
        sc.parallelize(top100).saveAsTextFile(args(1)+"11")

        println("SriramFinalPageRank-"+PRAcc.value+"N:"+N)
      }
    }
  }

}
