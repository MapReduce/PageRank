package wiki;

import java.io.IOException;
//import java.util.HashSet;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class OutputMapper extends Mapper<Text, PRNode, Text, Text> {
//	HashSet<String> set;
//	public void setup(Context ctx) {
//		set = new HashSet<String>();
//	}
//	public void cleanup(Context ctx) throws IOException, InterruptedException {
//		
//	}
	public void map(Text nid, PRNode node, Context ctx) throws InterruptedException, IOException {
		ctx.write(nid,new Text(node.toString()));
	}
}
