package wiki;
import java.io.IOException;
import java.util.List;


import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PRMapper  extends Mapper<Text, PRNode, Text, PRNode> {
	
	public void map(Text nid, PRNode node, Context ctx) throws InterruptedException, IOException {
		//pass the graph
		node.setNode(true);
		ctx.write(nid,node);
		List<String> adjList = node.getAdjList();
		if(adjList == null){
			return;
		}
		//compute PageRank distribution for each outlink
		double pr = node.getPagerank()/adjList.size();
		for(int ii = 0; ii < adjList.size(); ii++){
			//just distribute pagerank this is not node.
			String adjNode = adjList.get(ii);
			PRNode newnode = new PRNode();
			newnode.setAdjList(null);
			newnode.setNode(false);
			newnode.setPagerank(pr);
			ctx.write(new Text(adjNode), newnode);
		}
	}
}
