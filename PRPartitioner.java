package wiki;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class PRPartitioner extends Partitioner<Text,PRNode> {

	@Override
	public int getPartition(Text html, PRNode node, int nprt) {
		return Math.max(Math.floorMod(html.toString().hashCode(), nprt), 0);
	}

}
