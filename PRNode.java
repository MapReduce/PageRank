package wiki;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.io.Writable;


public class PRNode implements Writable{
	//page rank of the node.
	Double pagerank;
	//adjacency list of the node.
	List<String> adjList;
	//true if this is a node false if its a page rank.
	boolean isNode;
	PRNode(){
		//default values assigned automatically...
	}
	
	public Double getPagerank() {
		return pagerank;
	}

	public void setPagerank(Double pagerank) {
		this.pagerank = pagerank;
	}

	public List<String> getAdjList() {
		return adjList;
	}

	public void setAdjList(List<String> adjList) {
		this.adjList = adjList;
	}

	public boolean isNode() {
		return isNode;
	}

	public void setNode(boolean isNode) {
		this.isNode = isNode;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		// TODO Auto-generated method stub
		pagerank = in.readDouble();
		int n = in.readInt();
		if(n > 0){
			adjList = new LinkedList<String>();
			for(int i = 0; i < n; i++)
				adjList.add(in.readUTF());
		}
		isNode = in.readBoolean();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		// TODO Auto-generated method stub
		out.writeDouble(pagerank);
		if(adjList != null){
		out.writeInt(adjList.size());
		for(String s : adjList)
			out.writeUTF(s);		
		}
		else{
			out.writeInt(0);
		}
		out.writeBoolean(isNode);
	}
	@Override
	public String toString(){
		//for testing
		return "PageRank"+pagerank;
	}
	
}
