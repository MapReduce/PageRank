
DIR := $(shell basename `pwd`)

$(DIR).jar: *.java build.gradle Makefile
	gradle build
	gradle shadowJar
	cp build/libs/$(DIR)-all.jar $(DIR).jar

run: $(DIR).jar
	hadoop jar $(DIR).jar /home/osboxes/Downloads/wiki*.bz2 ./itr ./output 2>&1| tee hadoop.log

clean:
	rm -rf build $(DIR).jar output itr? itr?? .gradle


.PHONY: run clean 

