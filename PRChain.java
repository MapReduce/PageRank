package wiki;

import java.util.TreeMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class PRChain {
	public static enum NC{
		NODES,
		DELTA,
		PR
	};	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		
		Path input = null, output = null, tmpoutput = null;
		if (otherArgs.length > 2) {
			input = new Path(otherArgs[0]);
			output = new Path(otherArgs[1]+"0");
			tmpoutput = new Path(otherArgs[1]+"00");
		}
		else {
			System.out.println("Not enough parameters. Need 3 folders as args");
			System.exit(2);
		}
		
		long N;
		double d;
		//get Node size from parser
		N = parser(conf, input, tmpoutput);
		//get dangling nodes' contribution delta also does preprocessing.
		//sets page rank to 1/N
		d = preprocess(conf, tmpoutput, output, N);
		int ii = 1;
		while (ii < 11) {
			//for each iteration pass values accumulated so far to the next iteration
			input = new Path(otherArgs[1]+(ii-1));
			output = new Path(otherArgs[1]+ii);
			//System.out.println("Sriram Delta "+d);
			Double delta = pagerank(conf, input, output, N, d);
			d = delta;
			ii++;
		}
		//final output job to print 100 pages
		input = new Path(otherArgs[1]+(ii-1));
		output = new Path(otherArgs[2]);
		writeSortedOutput(conf, input, output);
		//writeOutput(conf, input, output);
	}
	static long parser(Configuration conf, Path input, Path tmpoutput) throws Exception{
		Job job = Job.getInstance(conf, "parser");
		job.setJarByClass(PRChain.class);
		
		job.setMapperClass(ParserMapper.class);
		job.setReducerClass(Reducer.class);
		job.setPartitionerClass(PRPartitioner.class);
		//job.setNumReduceTasks(5);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(PRNode.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		
		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, tmpoutput);

		boolean ok = job.waitForCompletion(true);
		if (!ok) {
			throw new Exception("Job failed");
		}
		long c = (long)job.getCounters().findCounter(NC.NODES).getValue();
		return c;
	}
	static double preprocess(Configuration conf, Path tmpoutput, Path output, long c) throws Exception {
		conf.setLong("N", c);
		Job job1 = Job.getInstance(conf, "Preprocess");
		job1.setJarByClass(PRChain.class);
		
		job1.setMapperClass(InitMapper.class);
		job1.setReducerClass(Reducer.class);
		job1.setPartitionerClass(PRPartitioner.class);
		//job1.setNumReduceTasks(5);
		
		job1.setOutputKeyClass(Text.class);
		job1.setOutputValueClass(PRNode.class);
		job1.setInputFormatClass(SequenceFileInputFormat.class);
		job1.setOutputFormatClass(SequenceFileOutputFormat.class);
		
		FileInputFormat.addInputPath(job1, tmpoutput);
		FileOutputFormat.setOutputPath(job1, output);

		boolean ok1 = job1.waitForCompletion(true);
		if (!ok1) {
			throw new Exception("Job failed");
		}
		long d = (long)job1.getCounters().findCounter(NC.DELTA).getValue();
		//convert it to double here, since we can increment global values in long
		return ((double)d)/100000000000000L;
	}
	static double pagerank(Configuration conf, Path input, Path output, long c, double delta) throws Exception{
		//delta and N are the values that we got in previous iterations.
		//N is computed once and doesnt chagne.
		//damping factor also doesnt change
		//delta accumulates and changes in each iteration.
		conf.setLong("N", c);
		conf.setDouble("damping", 0.15);
		conf.setDouble("Delta", delta);
		Job job = Job.getInstance(conf, "Page Rank");
		job.setJarByClass(PRChain.class);
		job.setMapperClass(PRMapper.class);
		job.setReducerClass(PRReducer.class);
		job.setPartitionerClass(PRPartitioner.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(PRNode.class);
		job.setOutputFormatClass(SequenceFileOutputFormat.class);
		job.setInputFormatClass(SequenceFileInputFormat.class);
		//job.setNumReduceTasks(5);
		
		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);
		boolean ok = job.waitForCompletion(true);
		if (!ok) {
			throw new Exception("Job failed");
		}
		long d = (long)job.getCounters().findCounter(NC.DELTA).getValue();
		//long prsum = (long)job.getCounters().findCounter(NC.PR).getValue();
		//System.out.println("Sriram "+((double)prsum)/100000000000000L);
		//System.out.println("Sriram delta "+((double)d)/100000000000000L);
		
		//convert it to double here, since we can increment global values in long
		return ((double)d)/100000000000000L;
		
	}
	//ignore
	static void writeOutput(Configuration conf, Path input, Path output) throws Exception{
		Job job = Job.getInstance(conf, "Output");
		job.setJarByClass(PRChain.class);
		job.setMapperClass(OutputMapper.class);
		job.setReducerClass(Reducer.class);
		job.setPartitionerClass(PRPartitioner.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		job.setInputFormatClass(SequenceFileInputFormat.class);
		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);
		
		boolean ok = job.waitForCompletion(true);
		if (!ok) {
			throw new Exception("Job failed");
		}
	}
	static void writeSortedOutput(Configuration conf, Path input, Path output) throws Exception{
		Job job = Job.getInstance(conf, "Output");
		job.setJarByClass(PRChain.class);
		job.setMapperClass(OutputSortMapper.class);
		job.setReducerClass(OutputSortReducer.class);
		//job.setPartitionerClass(PRPartitioner.class);
		job.setMapOutputKeyClass(DoubleWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(DoubleWritable.class);
		job.setOutputValueClass(Text.class);
		//for the purpose of global top 100
		job.setNumReduceTasks(1);
		job.setInputFormatClass(SequenceFileInputFormat.class);
		FileInputFormat.addInputPath(job, input);
		FileOutputFormat.setOutputPath(job, output);
		
		boolean ok = job.waitForCompletion(true);
		if (!ok) {
			throw new Exception("Job failed");
		}
	}
}
