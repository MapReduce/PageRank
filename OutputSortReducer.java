package wiki;

import java.io.IOException;
import java.util.TreeMap;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class OutputSortReducer extends Reducer<DoubleWritable, Text, DoubleWritable, Text> {
	//treemap orders entries by key.
	TreeMap<Double, String> top;
	
	@Override
	public void setup(Context ctx){
		top = new TreeMap<Double,String>();
	}
	@Override
	public void reduce(DoubleWritable key, Iterable<Text> vals, Context ctx) throws IOException, InterruptedException {
		//find global top 100
		for(Text t : vals){
			top.put(key.get(),t.toString());
			if(top.size() > 100){
				//we already have 100 elements so remove the least.
				top.remove(top.firstKey());
			}						
		}
		
	}
	@Override
	public void cleanup(Context ctx) throws IOException, InterruptedException{
		int i = 1;
		//Global top 100 with ranks
		for(double d : top.descendingKeySet()){
			ctx.write(new DoubleWritable(d),new Text(top.get(d)+"\t"+i));
			i++;
		}
	}
	
}