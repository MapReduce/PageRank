package wiki;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import wiki.PRChain.NC;

public class PRReducer extends Reducer<Text, PRNode, Text, PRNode> {
	double damp;
	long N;
	double delta;
	
	@Override
	public void setup(Context ctx){
		//get the values passed by conf only once. accessing conf takes time.
		damp = ctx.getConfiguration().getDouble("damping", 0.15);
		N = ctx.getConfiguration().getLong("N", 1);
		delta = ctx.getConfiguration().getDouble("delta", 0.0);
		
	}
	
	@Override
	public void reduce(Text nid, Iterable<PRNode> vals, Context ctx) throws IOException, InterruptedException {
		PRNode m = null;
		double s = 0;
		//int i = 0;
		Iterator<PRNode> itr = vals.iterator();
		while(itr.hasNext()){
			PRNode val = itr.next();
			if(val.isNode()){
				//recovered the node.
				m = val;
			}
			else{
				//not the node so we accumulate the page rank sum.
				s+=val.getPagerank();
				//i++;
			}
		}
		if(m != null){
			//page rank formula
			double pr = (damp/N) + (1-damp)*((delta/N)+s);
			m.setPagerank(pr);
			if(m.getAdjList() != null && m.getAdjList().size() == 0){
				//dangling node so accumulate Delta.
				ctx.getCounter(NC.DELTA).increment((long)(pr*100000000000000L));
			}
			//just to check if page rank accumulates to 1.
			ctx.getCounter(NC.PR).increment((long)(pr*100000000000000L));
			ctx.write(nid, m);
		}
		else{
			//deadnodes
			//System.out.println("Strange delta"+ctx.getConfiguration().getDouble("delta", 0.0));
		}
	}
}
