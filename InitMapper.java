package wiki;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import wiki.PRChain.NC;

public class InitMapper extends Mapper<Text, PRNode, Text, PRNode>   {
	int N;
	@Override
	public void setup(Context ctx){
		N = ctx.getConfiguration().getInt("N", 1);
		//System.out.println("Sriram N"+N);
	}
	@Override
	public void map(Text nid, PRNode node, Context ctx) throws InterruptedException, IOException {
		//set page rank to 1/N for all pages.
		node.setPagerank(((double)1)/N);
		if(node.getAdjList() != null && node.getAdjList().size() == 0){
			//delta accumulation
			ctx.getCounter(NC.DELTA).increment((long)(((double)1)/N)*100000000000000L);
		}
		
		ctx.write(nid, node);
	}	
}